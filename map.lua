local bit = require("utils.math.bit")

Map = {}
MapMT = { __index = Map }

function Map:create(w, h)
  assert(w and h, "undefined width/height")
	local self = {}
	setmetatable(self, MapMT)
  self.range = { l = 1, t = 1, r = 1, b = 1 }
  self.data = {}
  for i = 1, w*h do
    self.data[i] = 0
  end
  self.w = w
  self.h = h
  self.layer = Layer()
  self.sprites = {}
  self.spool = {}
  return self
end

function Map:destroy()
  self.range = nil
  self.data = nil
  self.w = nil
  self.h = nil
  self.layer = nil
  self.sprites = nil
  self.spool = nil
end

function Map:dimensions()
  return self.w, self.h
end

function Map:size()
  return self.w*self.h
end

-- converts index to cell
function Map:cell(n)
  n = n - 1
  local x = n%self.w
  local y = ((n - x)/self.w)%self.h
  return x + 1, y + 1
end

-- converts cell to index
function Map:index(x, y)
  x = (x - 1)%self.w
  y = (y - 1)%self.h
  return y*self.w + x + 1
end

-- checks if a cell is on the map
function Map:vcell(x, y)
  if x < 1 or x > self.w then
    return false
  end
  if y < 1 or y > self.h then
    return false
  end
  return true
end

-- bit ops
function Map:get(x, y, b)
  local i = self:index(x, y)
  local n = self.data[i]
  return bit.get(n, b)
end

function Map:set(x, y, b)
  local i = self:index(x, y)
  local n = self.data[i]
  self.data[i] = bit.set(n, b)
end

function Map:isset(x, y, b)
  local i = self:index(x, y)
  local n = self.data[i]
  return bit.isset(n, b)
end

function Map:unset(x, y, b)
  local i = self:index(x, y)
  local n = self.data[i]
  self.data[i] = bit.unset(n, b)
end

function Map:adjacent(x, y, b, dirs)
  local n = 0
  for i, v in ipairs(dirs) do
    local x2 = x + v[1]
    local y2 = y + v[2]
    if map:vcell(x2, y2) and map:isset(x2, y2, b) then
      --n = n + bit.set(n, i - 1)
      n = n + 2^(i - 1)
    end
  end
  return n
end

-- N, E, S, W
--     __      |  |      __      |  
-- 0. |__|  1. |__|  2. |__   3. |__
--
--     __      |  |      __      |  
-- 4. |  |  5. |  |  6. |     7. |  
--
--     __         |     ___         
-- 8.  __|  9.  __| 10. ___  11. ___
--
--     __         |     ___         
-- 12.   |  13.   | 14.      15.    

local dirs4 = { {0,1}, {1,0}, {0,-1}, {-1,0} }
function Map:adjacent4(x, y, b)
  return self:adjacent(x, y, b, dirs4)
end

-- N, NE, E, SE, S, SW, W, NW
local dirs8 = { {0,1}, {1,1}, {1,0}, {1,-1}, {0,-1}, {-1,-1}, {-1,0}, {-1,1} }
function Map:adjacent8(x, y, b)
  return self:adjacent(x, y, b, dirs8)
end

-- set loaded range (in cells)
function Map:setRange(sx, sy, ex, ey)
  assert(sx < ex and sy < ey, "misaligned range")
  -- clamp
  sx, ex = math.max(sx, 1), math.min(ex, self.w)
  sy, ey = math.max(sy, 1), math.min(ey, self.h)
  -- current range
  local range = self.range
  local l, t = range.l, range.t
  local r, b = range.r, range.b
  -- range hasn't changed?
  if l == sx and t == sy and r == ex and b == ey then
    return
  end
  -- load new range
  for x = sx, ex do
    for y = sy, ey do
      if x < l or x > r or y < t or y > b then
        self:initCell(x, y)
      end
    end
  end
  -- unload old range
  --l, t = math.min(l, sx), math.min(t, sy)
  --r, b = math.max(r, ex), math.max(b, ey)
  for x = l, r do
    for y = t, b do
      if x < sx or x > ex or y < sy or y > ey then
        self:releaseCell(x, y)
      end
    end
  end
  range.l, range.t = sx, sy
  range.r, range.b = ex, ey
end

-- set loaded range (in pixels)
function Map:setRangeP(sx, sy, ex, ey)
  local sx, sy = self:posToCell(sx, sy)
  local ex, ey = self:posToCell(ex, ey)
  self:setRange(sx, sy, ex, ey)
end

-- assign a spritesheet for the map
function Map:setSpritesheet(fn, r, c, tw, th)
  display.images.load_sheet(fn, r, c)
  map.tw = tw
  map.th = th
  map.canvases = {}
  for i = 1, r*c do
    local n = string.format("%s.%d", fn, i - 1)
    local img = display.images.find(n)
    map.canvases[i] = Canvas()
    display.images.paint_at(map.canvases[i], img, 0, 0)
  end
end

-- assign a tile to a given cell
function Map:setTile(x, y, c)
  local i = self:index(x, y)
--terminal.log("%dx%d=%d", x, y, c)
  assert(self.sprites[i], "cannot set a tile: position out of range")
  self.sprites[i].canvas = self.canvases[c]
end

-- cell positioning
function Map:posToCell(wx, wy)
  local tw = map.tw
  local th = map.th
  -- bottom left corner of cell
  wx, wy = wx - tw/2, wy - th/2
  return math.floor(wx/tw) + 1, math.floor(wy/th) + 1
end

-- get the center position of a cell
function Map:cellPosC(cx, cy)
  return cx*map.tw, cy*map.th
end

-- get the top left corner of a cell
function Map:cellPosTL(cx, cy)
  local tw = map.tw
  local th = map.th
  return cx*tw - tw/2, cy*th - th/2
end

function Map:initCell(x, y)
  local i = self:index(x, y)
  if self.sprites[i] == nil then
    local wx, wy = self:cellPosC(x, y)
    local s = table.remove(self.spool)
    if s == nil then
      s = Sprite()
    end
    s:set_position(wx, wy)
    self.sprites[i] = s
    self.layer:add_child(s)
  end
  self:onInitCell(x, y)
end

function Map:releaseCell(x, y)
  self:onReleaseCell(x, y)
  local i = self:index(x, y)
  if self.sprites[i] then
    local i = self:index(x, y)
    local s = self.sprites[i]
    self.layer:remove_child(s)
    s.canvas = nil
    self.sprites[i] = nil
    table.insert(self.spool, s)
  end
end

function Map:onInitCell(x, y)

end

function Map:onReleaseCell(x, y)

end