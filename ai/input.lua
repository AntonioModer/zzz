local keys =
{
  left = { KEY_LEFT, KEY_NUM_4 },
  right = { KEY_RIGHT, KEY_NUM_6 },
  up = { KEY_UP, KEY_NUM_8 },
  down = { KEY_DOWN, KEY_NUM_2 },
  jump = { KEY_Z },
  punch = { KEY_X }
}

local jbuttons =
{
  jump = { JBUTTON_1 },
  punch = { JBUTTON_2 }
}

local Input = {}
local InputMT = { __index = Input }

Input.create = function(self, mt)
	local self = State:create(InputMT)

  self.actions = {}
  self.oldactions = {}
  
  return self
end
Input.destroy = function(self)
  self.actions = nil
  self.oldactions = nil
  
  State.destroy(self)
end

Input.enter = function(self, agent)
end
Input.update = function(self, agent, dt)
  local actions = self.actions
  
  -- cache the current actions
  local oldactions = self.oldactions
  for action, state in ipairs(actions) do
    oldactions[action] = state
  end
  
  -- get input
  if keyboard then
    for action, keys in pairs(keys) do
      actions[action] = false
      for _, key in ipairs(keys) do
        actions[action] = actions[action] or keyboard:is_down(key)
      end
    end
  end
  if joystick then
    for action, jbuttons in pairs(jbuttons) do
      for _, button in ipairs(jbuttons) do
        actions[action] = actions[action] or joystick:is_down(button)
      end
    end
  end
  
  local sz = ""
  for i, v in pairs(actions) do
    sz = sz .. i .. "="
    if v == true then
      sz = sz .. "true "
    else
      sz = sz .. "false "
    end
  end
  terminal.trace("buttons", sz)

  local xm, ym = 0, 0
  if actions.left then
    xm = -1
  elseif actions.right then
    xm = 1
  end
  if actions.down then
    ym = -1
  elseif actions.up then
    ym = 1
  end
  agent:setMovement(xm, ym)

  if actions.punch and not oldactions.punch then
    -- todo
    agent:message("punch")
  end
  
  local jump = 0
  if actions.jump then
    jump = 1
  end
  agent:setJump(jump)
end
Input.exit = function(self, agent)
end

return Input