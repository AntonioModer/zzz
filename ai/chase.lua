local Chase = {}
local ChaseMT = { __index = Chase }

Chase.create = function(self, mt)
	local self = State:create(ChaseMT)

  return self
end
Chase.destroy = function(self)
  State.destroy(self)
end

Chase.enter = function(self, agent)
  self.target = p
end
Chase.update = function(self, agent, dt)
  local actions = agent.actions
  local ts = self.target:getShape()
  local s = agent:getShape()
  if ts.x < s.x then
    actions.left = true
    actions.right = false
  else
    actions.left = false
    actions.right = true
  end
end
Chase.exit = function(self, agent)
end

return Chase