display:create("ZZZ", 800, 600, 32, true)

require("fizzx.fizz")

fizz.gravity = -700
quad.mincellsize = 8

require("utils.log.terminal")
--require("utils.image")
require("utils.agen.display")
require("utils.agen.images")

-- init states
require("lfs")
require("zzz.states.state")

local path = "zzz/states"
states = {}
for file in lfs.dir(path) do
  if file ~= "." and file ~= ".." then
    local full = string.format("%s/%s", path, file)
    local i = string.sub(file, 1, -5)
    states[i] = dofile(full)
  end
end

local path = "zzz/ai"
goals = {}
for file in lfs.dir(path) do
  if file ~= "." and file ~= ".." then
    local full = string.format("%s/%s", path, file)
    local i = string.sub(file, 1, -5)
    goals[i] = dofile(full)
  end
end

require("zzz.map")
require("zzz.agent")
require("zzz.player")

bmp = require("utils.io.bmp")
colors = require("utils.agen.colors")

sq = 8

scene = Layer()

display.images.load_sheet("zzz/player.png", 16, 16, LIME)
display.images.load("zzz/water.png", LIME)

-- loading procedure
local file = bmp.open("zzz/map.bmp")
bmp.parse(file)
local w, h = file.biWidth, file.biHeight
local data = file.data

map = Map:create(w, h)
map:setSpritesheet("zzz/walls.png", 4, 4, 8, 8)
scene:add_child(map.layer)

map:set(2, 1, 0)
map:set(2, 3, 0)
map:set(1, 2, 0)
map:set(3, 2, 0)
assert(map:isset(2, 1, 0))
assert(map:isset(2, 3, 0))
assert(map:isset(1, 2, 0))
assert(map:isset(3, 2, 0))
assert(map:adjacent4(2, 2, 0) == 15)

bits = { solid = 0, breakable = 1 }
for i, n in ipairs(data) do
  local r, g, b = colors.hex_to_rgb(n)
  if r == 0 and g == 0 and b == 0 then
    local x, y = map:cell(i)
    map:set(x, y, bits["solid"])
    map:set(x, y, bits["breakable"])
    assert(map:isset(x, y, bits["solid"]))
    assert(map:isset(x, y, bits["breakable"]))
  end
end
shapes = {}

local dirs4 = { {0,1}, {1,0}, {0,-1}, {-1,0} }
function map:breakCell(x, y)
  if not self:isset(x, y, bits["breakable"]) then
    return false
  end

  self:unset(x, y, bits["breakable"])
  self:unset(x, y, bits["solid"])

  self:releaseCell(x, y)
  for _, v in ipairs(dirs4) do
    self:initCell(x + v[1], y + v[2])
  end
  return true
end

function map:onInitCell(cx, cy)
  local i = self:index(cx, cy)
  -- nothing to load?
  --if data[i] == nil or data[i] == 0 then
  if not map:isset(cx, cy, bits["solid"]) then
    return
  end
  local av = map:adjacent4(cx, cy, bits["solid"])
--assert(av >= 0 and av <= 15)
  self:setTile(cx, cy, av + 1)
  
  if self:isset(cx, cy, bits["solid"]) then
    if av < 15 then
      -- center of the tile
      local wx, wy = self:cellPosC(cx, cy)
      local s = shapes[i]
      if s == nil then
        s = fizz.addStatic("rect", wx, wy, self.tw, self.th)
      end
      s.top = av%2 >= 1
      s.right = av%4 >= 2
      s.bottom = av%8 >= 4
      s.left = av%16 >= 8
      shapes[i] = s
    end
  end
end

function map:onReleaseCell(cx, cy)
  local i = self:index(cx, cy)
  local s = shapes[i]
  if s then
    fizz.removeShape(s)
    shapes[i] = nil
  end
end


local camera = Camera()
scene:add_child(camera)
camera:set_scale(0.5, 0.5)
--camera:set_scale(1, 1)

display.viewport.camera = camera

agents = {}

local x, y = 61, 256-64
camera:set_position(x*sq, y*sq)

p = Player:create(x*sq, y*sq)
p:setAI("input")
scene:add_child(p.sprite)
table.insert(agents, p)

mouse.on_wheelmove = function(m, z)
  if z < 0 then
    z = 1.1
  else
    z = 0.9
  end
  local s = camera.scalex
  --camera:set_scale(s*z, s*z)
end

function keyboard:on_press(key)
  if key == KEY_ESCAPE then
    paused = not paused
  end
end

paused = false

t = Timer()
t.accum = 0
t.elapsed = 0
t.sync = 0
t.ticks = 0
t:start(16, true)
t.on_tick = function(t)
  if paused == true then
    return
  end
  local dt = t:get_delta_ms()/1000
  t.elapsed = t.elapsed + dt*8
  t.accum = t.accum + dt

  local it = t.interval/1000
  while t.accum > it do
    t.ticks = t.ticks + 1
    t.accum = t.accum - it
    fizz.update(it)
    for _, v in pairs(agents) do
      v:update(it)
    end
  end
  for _, v in pairs(agents) do
    v:sync(dt)
  end

  local s = p.shape
  local dx, dy = s.x - camera.x, s.y - camera.y
  local dist = math.sqrt(dx*dx + dy*dy)
  if dist > 0 then
    local nx, ny = dx/dist, dy/dist 
    local speed = math.min(dist/100, 1)*300
    local step = speed*dt
    if step <= dist then
      camera:change_position(nx*step, ny*step)
    end
  end

  --camera:set_position(s.x, s.y)

  --[[
  local w, h = map:dimensions()
  terminal.trace("statics", #fizz.statics)
  terminal.trace("dynamics", #fizz.dynamics)
  terminal.trace("world", w .. "," .. h)
  terminal.trace("cchecks", fizz.cchecks)
  terminal.trace("mshapes", fizz.mshapes)
  terminal.trace("livecells", quad.livecells)
  terminal.trace("cellpool", #quad.deadcells)
  ]]
  terminal.grapht("cchecks", fizz.cchecks)
  terminal.grapht("mshapes", fizz.mshapes)
  terminal.grapht("statics", #fizz.statics)
  terminal.grapht("dynamics", #fizz.dynamics)

  --local s = p.shape
  --terminal.graph("hspeed", math.abs(s.xv)/fizz.maxVelocity)
  --terminal.graph("vspeed", math.abs(s.yv)/fizz.maxVelocity)
  
  --terminal.graph("received", received)
  --terminal.graph("sent", sent)
  
  map:setRangeP(s.x - 100, s.y - 100, s.x + 100, s.y + 100)
end
