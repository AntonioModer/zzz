local states = {}

states.standing = {}
function states.standing:enter(agent)
  agent:setStance("standing")
end
function states.standing:update(agent, dt)
  local jump = agent:getJump()
  if jump > 0 then
    agent:setState("jumping")
    return
  end
  local xmove, ymove = agent:getMovement()
  if xmove ~= 0 then
    local dir = agent:getDirection()
    if (xmove < 0 and dir < 0) or (xmove > 0 and dir > 0) then
      agent:setState("stopping")
    else
      agent:setState("running")
    end
  elseif ymove == -1 then
    self:setState("ducking")
  end
end
function states.standing:message(agent, msg)
  if msg == "fall" then
    agent:setState("falling")
  elseif msg == "punch" then
    agent:setState("punching")
  end
end

states.running = {}
function states.running:enter(agent)
  self.dir = agent:getDirection()
end
function states.running:update(agent, dt)
  local dir = agent:getDirection()
  local accel = agent:getDefinition("move")
  local s = agent:getShape()
  s.xv = s.xv + accel*dir
  assert(s.friction > 0)
  if self.dir ~= dir then
    agent:message("turn")
  end
end
function states.running:exit(agent)
  self.dir = nil
end
function states.running:message(agent, msg)
  if msg == "fall" then
    agent:setState("falling")
  elseif msg == "jump" then
    agent:setState("jumping")
  elseif msg == "push" then
    agent:setState("pushing")
  elseif msg == "punch" then
    agent:setState("punching")
  elseif msg == "stop" then
    agent:setState("stopping")
  elseif msg == "climb" then
    agent:setState("climbing")
  elseif msg == "down" then
    agent:setState("ducking")
  elseif msg == "turn" then
    agent:setState("turning")
  end
end

states.falling = {}
function states.falling:enter(agent)
  agent:setStance("flying")
end
function states.falling:update(agent, dt)
  local elapsed = agent:getElapsed()
  if elapsed > 1 then
    agent:setState("diving")
  end
  local s = agent:getShape()
  -- horizontal movement
  local xmove, ymove = agent:getMovement()
  local accel = agent:getDefinition("airmove")
  s.xv = s.xv + accel*xmove--*dt
end
function states.falling:message(agent, msg)
  if msg == "hitground" then
    agent:setState("landing")
  elseif msg == "down" then
    agent:setState("diving")
  elseif msg == "punch" then
    agent:setState("kicking")
  end
end