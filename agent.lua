Agent = {}
AgentMT = { __index = Agent }

function Agent:create(wx, wy, mt)
	local self = {}
	setmetatable(self, mt or AgentMT)

  -- visuals
  self.sprite = Sprite(wx, wy)
  self.facing = 1
  -- displacement
  self.xd, self.yd = 0, 0
  -- movement
  self.xmove, self.ymove = 0, 0
  self.jump = 0
  -- state
  self.grounded = false
  self.elapsed = 0

  return self
end
function Agent:destroy()
  self.sprite = nil
  self.facing = nil
  self.xd, self.yd = nil, nil
  self.xmove, self.ymove = nil, nil
  self.jump = nil
  self.grounded = nil
  self.elapsed = nil
end

function Agent:setShape(s)
  assert(self.shape == nil)
  self.shape = s
  s.onCollide = function(s, b, nx, ny, pen)
    -- add up displacement vectors
    self.xd = self.xd + nx*pen
    self.yd = self.yd + ny*pen
    return true
  end
end
function Agent:getShape()
  return self.shape
end

-- get the facing direction
function Agent:getDirection()
  return self.facing
end
function Agent:setDirection(dir)
  self.facing = dir
end

-- get/set the movement direction of the agent
function Agent:getMovement()
  return self.xmove, self.ymove
end
function Agent:setMovement(x, y)
  self.xmove = x
  self.ymove = y
end

-- get/set the jump of the agent
function Agent:getJump()
  return self.jump
end
function Agent:setJump(j)
  self.jump = j
end

function Agent:getPosition()
  return self.shape.x, self.shape.y
end
function Agent:setPosition(x, y)
  return fizz.positionShape(self.shape, x, y)
end

function Agent:getVelocity()
  return fizz.getVelocity(self.shape)
end
function Agent:setVelocity(vx, vy)
  return fizz.setVelocity(self.shape, vx, vy)
end

function Agent:getDisplacement()
  return self.xd, self.yd
end
function Agent:setDisplacement(xd, yd)
  self.xd = xd
  self.yd = yd
end

-- get the elapsed time of the current state
function Agent:getElapsed()
  return self.elapsed
end

function Agent:setState(state)
  if state == self.statesz then
    return
  end
  if self.state then
    self.state:exit(self)
  end
  assert(states[state], state)
  -- state info
  self.elapsed = 0
  self.statesz = state
  self.state = states[state]:create()
  self.state:enter(self)
end
function Agent:updateState(dt)
  self.elapsed = self.elapsed + dt
  self.state:update(self, dt)
end
function Agent:message(msg, ...)
  self.state:message(self, msg, ...)
end

function Agent:update(dt)
  -- current movement, velocity and displacement
  local xm, ym = self:getMovement()
  local xv, yv = self:getVelocity()
  local xd, yd = self:getDisplacement()
  local jump = self:getJump()

  -- pushing against wall?
  local pushleft = xm < 0 and xd > 0
  local pushright = xm > 0 and xd < 0
  if pushleft or pushright then
    --if self.xdold == 0 then
      if self:canClimbLedge() and self:canStand() then
        self:message("climb")
      else
        self:message("push")
      end
    --end
  end

  local isgrounded = yd > 0
  local wasgrounded = self.grounded
  self.grounded = isgrounded
  if isgrounded and not wasgrounded then
    self:message("hitground")
  end
  if not isgrounded and wasgrounded then
    self:message("fall")
  end
  if yd < 0 and yv > 0 then
    self:message("hitroof")
  end
  
  self:updateState(dt)
  
  self:setDisplacement(0, 0)
end

-- tilesheet is assumed to be 8x8
function Agent:setAnimations(ts, a)
  self.tilesheet = ts
  self.animations = a
end

function Agent:setDefinitions(d)
  self.definitions = d
end

function Agent:getDefinition(n)
  return self.definitions[n]
end

function Agent:sync(dt)
  local state = self.statesz
  local a = self.animations[state]

  assert(a, state)

  -- update animation
  local frame = math.floor(self.elapsed/a.fps)
  if frame > a.frames - 1 then
    if a.loop == true then
      frame = frame%a.frames
    else
      frame = a.frames - 1
    end
  end
  
  local i = frame + a.offset
  local n = string.format("%s.%d", self.tilesheet, i)
  local img = display.images.find(n)

  local shape = self.shape
  local hw, hh = shape.hw, shape.hh
  local c = self.sprite.canvas
  c:clear()
  if img then
    display.images.paint_at(c, img, 0, sq - hh)
  end

  self.sprite.scalex = self.facing

  -- sync to shape
  self.sprite:set_position(shape.x, shape.y)
end