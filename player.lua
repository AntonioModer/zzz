local defs =
{
  -- shape
  damping = 2,
  friction = 0.3,
  width = 6,
  height = 12,
  -- acceleration
  move = 40,
  airmove = 40/6,
  crawl = 40/4,
  jump = 60,
  -- time
  jumptime = 0.3,
  backflip = 0.3*1.25,
  turn = 0.2
}

local animations =
{
  running = { offset = 0, frames = 6, fps = 0.08, loop = true },
  standing = { offset = 6, frames = 2, fps = 0.4, loop = true },
  falling = { offset = 8, frames = 2, fps = 0.08, loop = true },
  jumping = { offset = 10, frames = 4, fps = 0.06, loop = false },
  landing = { offset = 14, frames = 1, fps = 0.1, loop = false },
  pushing = { offset = 15, frames = 4, fps = 0.12, loop = true },
  crawling = { offset = 19, frames = 6, fps = 0.1, loop = true },
  ducking = { offset = 25, frames = 1, fps = 0.1, loop = false },
  climbing = { offset = 26, frames = 6, fps = 0.06, loop = false },
  punching = { offset = 32, frames = 5, fps = 0.06, loop = false },
  dpunching = { offset = 37, frames = 5, fps = 0.06, loop = false },
  backflip = { offset = 42, frames = 11, fps = 0.035, loop = false },
  stopping = { offset = 53, frames = 3, fps = 0.05, loop = false },
  turning = { offset = 56, frames = 3, fps = 0.08, loop = false },
  rolling = { offset = 59, frames = 9, fps = 0.06, loop = false },
  diving = { offset = 68, frames = 7, fps = 0.04, loop = false },
  crashing = { offset = 75, frames = 6, fps = 0.03, loop = false },
  rising = { offset = 81, frames = 5, fps = 0.08, loop = false },
  kicking = { offset = 87, frames = 3, fps = 0.05, loop = false },
  stumbling = { offset = 90, frames = 2, fps = 0.08, loop = true }
}

Player = {}
PlayerMT = { __index = Player }

setmetatable(Player, { __index = Agent })

Player.create = function(self, wx, wy)
	local self = Agent:create(wx, wy, PlayerMT)

  self.sprite.depth = -5
  local s = fizz.addDynamic("rect", wx, wy, defs.width, defs.height)
  s.damping = defs.damping
  s.friction = defs.friction
  self:setShape(s)
  self:setAnimations("zzz/player.png", animations)
  self:setDefinitions(defs)
  self:setState("falling")
  
  return self
end
Player.destroy = function(self)
  fizz.removeShape(self.shape)
  self:setShape(nil)
  self:setState(nil)
  self:setAnimations(nil, nil)
  Agent.destroy(self)
end

Player.setAI = function(self, ai)
  if self.ai then
    self.ai:exit(self)
  end
  assert(ai, "AI goal is nil")
  assert(goals[ai], "Invalid AI goal: " .. ai)
  self.ai = goals[ai]:create()
  self.ai:enter(self)
end
Player.updateAI = function(self, dt)
  if self.ai then
    self.ai:update(self, dt)
  end
end
Player.messageAI = function(self, msg, ...)
  if self.ai then
    self.ai:message(self, msg, ...)
  end
end

Player.update = function(self, dt)
  -- update ai
  self:updateAI(dt)

  local xm, ym = self:getMovement()
  if xm < 0 then
    self:setDirection(-1)
    self:message("move")
  elseif xm > 0 then
    self:setDirection(1)
    self:message("move")
  else
    self:message("stop")
  end
  if ym < 0 then
    self:message("down")
  elseif ym > 0 then
    self:message("up")
  end
--[[
  if actions.punch and not oldactions.punch then
    self:message("punch")
  end
  ]]
  if self.jump > 0 then
    self:message("jump")
  end
  
  Agent.update(self, dt)
end

Player.canStand = function(self)
  local s = self:getShape()
  local lx, ly = 0, (sq/2 + 2)
  local hw, hh = 2, 1
  local buffer = {}
  local a = { shape = "rect", x = s.x + lx, y = s.y + ly, hw = hw, hh = hh }
  fizz.queryShape(buffer, a)
  --fizz.queryRect(buffer, s.x + lx, s.y + ly, hw, hh)
  for i, v in ipairs(buffer) do
    if v == self.shape then
      table.remove(buffer, i)
      break
    end
  end
  return #buffer == 0
end

Player.canClimbLedge = function(self, x, y)
  local s = self:getShape()
  local lx, ly = (sq/2)*self.facing, (sq/2 + 3)
  local hw, hh = sq - 1, 2
  if self.stance == "ducking" then
    ly = ly + 2
    hh = hh + 1
  end
  local buffer = {}
  local a = { shape = "rect", x = s.x + lx, y = s.y + ly, hw = hw, hh = hh }
  fizz.queryShape(buffer, a)
  --fizz.queryRect(buffer, s.x + lx, s.y + ly, hw, hh)
  for i, v in ipairs(buffer) do
    if v == s then
      table.remove(buffer, i)
      break
    end
  end
  self.cols = buffer
  self.a = a
  return #buffer == 0
end

Player.setStance = function(self, stance)
  if stance == self.stance then
    return
  end

  local s = self.shape
  local defs = self.definitions
  if stance == "flying" then
    s.friction = 0
    s.hw = defs.width/2
    s.hh = defs.height/2
  elseif stance == "standing" then
    s.friction = defs.friction
    s.hw = defs.width/2
    s.hh = defs.height/2
    if self.stance == "ducking" then
      fizz.moveShape(s, 0, (defs.height/2 - defs.height/4))
      --s.y = s.y + (defs.height/2 - defs.height/4)
    end
  elseif stance == "ducking" then
    s.friction = defs.friction
    s.hw = defs.width/2
    s.hh = defs.height/4
    if self.stance == "standing" then
      fizz.moveShape(s, 0, -(defs.height/2 - defs.height/4))
      --s.y = s.y - (defs.height/2 - defs.height/4)
    end
  else
    assert("Unknown stance:", stance or "nil")
  end
  self.stance = stance
end