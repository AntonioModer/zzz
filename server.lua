server = true

dofile("ZZZ/main.lua")


--[[
socket = require("socket")
udp = socket.udp()
udp:settimeout(0)

clients = {}
username = nil
port = 14285

if server == true then
  -- bind UDP to all local interfaces
  udp:setsockname("*", port)
  username = "server"
else
  --udp:setpeername("localhost", port)
  udp:setpeername("178.239.235.207", port)
  --85.118.193.172 
  udp:send("hello:client")
  username = "client"
  connected = false
end

clients[username] = { agent = p }

function receive_msg(data, ip, port)
  local msg, sz = string.match(data, "(%w+):(.+)")
  --terminal.log((msg or "?") .. ":" .. (sz or "?"))
  if msg == "hello" then
    if server == true then
      if clients[sz] ~= nil then
        return
      end
      --assert(clients[sz] == nil, "Username taken:" .. sz)
      udp:sendto("welcome:ok", ip, port)
      udp:sendto("setticks:" .. t.ticks, ip, port)
      for i, v in pairs(clients) do
        udp:sendto("hello:" .. i, ip, port)
      end
    end
    local a = Player:create(x*sq, y*sq)
    --a:setAI("remote")
    scene:add_child(a.sprite)
    table.insert(agents, a)
    clients[sz] = { agent = a, ip = ip, port = port }
  elseif msg == "welcome" then
    assert(sz == "ok")
    terminal.log("welcome")
    connected = true
  elseif msg == "setticks" then
    local ticks = tonumber(sz)
    terminal.log(ticks)
    t.ticks = ticks
  elseif msg == "sync" or msg == "input" then
    local client, data = string.match(data, "(%w+);(.+)")
    if clients[client] == nil then
      return
    end
    local agent = clients[client].agent
    local m = {}
    for sz in string.gmatch(data, "[^;]+") do
      table.insert(m, sz)
    end
    local ticks = tonumber(m[1])
    if msg == "sync" then
      local lag = math.abs(ticks - t.ticks)
      local s = agent:getShape()
      --s.x, s.y = tonumber(m[2]), tonumber(m[3])
      --fizz.repartition(s)
      local x, y = tonumber(m[2]), tonumber(m[3])
      s.vx, s.vy = tonumber(m[4]), tonumber(m[5])
      --fizz.moveShape(s, x - s.x, y - s.y)
      agent.facing = tonumber(m[6])
      if client ~= username then
        agent.xmove, agent.ymove = tonumber(m[7]), tonumber(m[8])
        agent.jump = tonumber(m[9])
      end
      agent.xdold, agent.ydold = t[10] or 0, t[11] or 0
      agent.xvold, agent.yvold = t[12] or 0, t[13] or 0
      agent:setStance(m[14])
      s.x, s.y = x, y
      fizz.repartition(s)
      agent:setState(m[15])
      agent.elapsed = tonumber(m[16])-- + lag*(t.interval/1000))
      agent:sync(t.interval)
    elseif msg == "input" then
      agent.xmove, agent.ymove = tonumber(m[2]), tonumber(m[3])
      agent.jump = tonumber(m[4])
    end
  end
end

function receive_from_clients()
  -- receive incoming data from clients
  while true do
    local data, ip, port = udp:receivefrom()
    if data == nil then
      break
    end
    receive_msg(data, ip, port)
    --received = received + string.len(data)
  end
end

function receive_from_server()
  -- receive incoming data from clients
  while true do
    local data = udp:receive()
    if data == nil then
      break
    end
    receive_msg(data)
    --received = received + string.len(data)
  end
end

function send_to_clients()
  -- send sync data to clients
  for i, v in pairs(clients) do
    -- send to all clients except for server
    if i ~= "server" then
      for j, k in pairs(clients) do
        local p = k.agent
        local s = p:getShape()
        local sz = string.format("sync:%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;",
          j,
          t.ticks,
          s.x, s.y,
          s.vx or 0, s.vy or 0,
          p.facing,
          p.xmove, p.ymove,
          p.jump,
          p.xdold, p.ydold,
          p.xvold, p.yvold,
          p.stance or "",
          p.statesz or "",
          p.elapsed or 0)
        udp:sendto(sz, v.ip, v.port)
        --sent = sent + string.len(sz)
        --udp:sendto("setticks:" .. t.ticks, v.ip, v.port)
      end
    end
  end
end

function send_to_server()
  if not connected then
    udp:send("hello:" .. username)
    return
  end
  -- send agent data to server
  local s = p:getShape()
  local sz = string.format("input:%s;%s;%s;%s;%s;",
    username,
    t.ticks,
    p.xmove, p.ymove,
    p.jump)
  --terminal.log("Send: " .. sz)
  udp:send(sz)
  --sent = sent + string.len(sz)
end
]]