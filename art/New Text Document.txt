
--[[
local animations =
{
  running = { offset = 0, frames = 10, fps = 0.06, loop = true },
  standing = { offset = 10, frames = 2, fps = 0.4, loop = true },
  falling = { offset = 12, frames = 2, fps = 0.08, loop = true },
  jumping = { offset = 14, frames = 2, fps = 0.08, loop = false },
  landing = { offset = 16, frames = 1, fps = 0.1, loop = false },
  pushing = { offset = 17, frames = 6, fps = 0.12, loop = true },
  crawling = { offset = 23, frames = 4, fps = 0.1, loop = true },
  ducking = { offset = 27, frames = 1, fps = 0.1, loop = false }
}
]]


lightimg = Image()
--assert(lightimg:load("zzz/light.png"))
overlay = Sprite()
overlay.depth = -10000
display.viewport:add_child(overlay)
overlay.canvas:set_source_image(lightimg)
--overlay.canvas:paint()

overlay.visible = false
devoverlay.visible = false

keyboard.on_press = function(k, b)
  if b == KEY_F1 then
    terminal.terminal.visible = not terminal.terminal.visible
  elseif b == KEY_F2 then
    overlay.visible = not overlay.visible
    devoverlay.visible = not devoverlay.visible
  end
end

local rectc =
{
  { x = -1, y = 1 },
  { x = 1, y = 1 },
  { x = 1, y = -1 },
  { x = -1, y = -1 }
}

local rectcc =
{
  { {1,-1}, {-1,-1}, {-1,1}, {1,1} },
  { {1,1}, {-1,1}, {-1,1}, {1,1} },
  { {1,-1}, {1,-1}, {1,1}, {1,1} },
  { {1,1}, {-1,-1}, {1,1}, {1,1} },
  
  { {1,-1}, {-1,-1}, {-1,-1}, {1,-1} },
  { {1,1}, {-1,1}, {-1,-1}, {1,-1} },
  { {1,-1}, {1,-1}, {-1,1}, {1,-1} },
  { {1,1}, {-1,-1}, {-1,1}, {1,-1} },
  
  { {-1,-1}, {-1,-1}, {-1,1}, {-1,1} },
  { {1,-1}, {-1,1}, {-1,1}, {-1,1} },
  { {-1,-1}, {1,-1}, {1,1}, {-1,1} },
  { {1,-1}, {-1,-1}, {1,1}, {-1,1} },
  
  { {-1,-1}, {-1,-1}, {-1,-1}, {1,1} },
  { {1,1}, {-1,1}, {-1,-1}, {1,-1} },
  { {-1,-1}, {1,-1}, {1,1}, {-1,1} },
  { {-1,1}, {1,1}, {1,-1}, {-1,-1} }
}

function redrawShadows(c, wox, woy, list, color, alpha)
  local cast = 300
  c:set_fill_style(color, alpha)
  c:set_line_style(0.5, RED, 1)

  for i, s in ipairs(list) do
    local x, y = map:posToCell(s.x, s.y)
    if s.x > wox - 200 and s.x < wox + 200 then
      local a = map.adj[x][y]
      if a and a < 15 then
        local t = s.shape
        if t == 'rect' then
          local w, h = s.hw, s.hh
          local wax = s.x + w*rectc[4].x
          local way = s.y + h*rectc[4].y
          wax = wax + rectcc[a + 1][4][1]*2
          way = way + rectcc[a + 1][4][2]*2
          --local dx, dy = wax - wox, way - woy
          --local wpax, wpay = dx + wax, dy + way

          local wpax, wpay = math.project(wox, woy, wax, way, cast)

          for i = 1, 4 do
            local wbx = s.x + w*rectc[i].x
            local wby = s.y + h*rectc[i].y

            wbx = wbx + rectcc[a + 1][i][1]*2
            wby = wby + rectcc[a + 1][i][2]*2
            
            local nx, ny = wby - way, -(wbx - wax)
            local lx, ly = wbx - wox, wby - woy
            
            --local dx, dy = wbx - wox, wby - woy
            --local wpbx, wpby = dx + wbx, dy + wby
            local wpbx, wpby = math.project(wox, woy, wbx, wby, cast)
            
            -- are we facing the edge?
            if math.dot(nx, ny, lx, ly) > 0 then
              c:move_to(wax, way)
              c:line_to(wpax, wpay)
              c:line_to(wpbx, wpby)
              c:line_to(wbx, wby)
              c:fill()
            end
            wax, way = wbx, wby
            wpax, wpay = wpbx, wpby
          end
        elseif t == 'circle' then
          local r = s.r
          local wpx, wpy = s.x, s.y
          local dx, dy = wpx - wox, wpy - woy
          local a = math.atan2(dy, dx)
          local nx, ny = math.cos(a)*r, math.sin(a)*r
          nx, ny = -ny, nx
          local wax, way = wpx + nx, wpy + ny
          local wbx, wby = wpx - nx, wpy - ny

          --local dx, dy = wax - wox, way - woy
          --local wpax, wpay = dx + wax, dy + way
          --local dx, dy = wbx - wox, wby - woy
          --local wpbx, wpby = dx + wbx, dy + wby
          
          local wpax, wpay = math.project(wox, woy, wax, way, cast)
          local wpbx, wpby = math.project(wox, woy, wbx, wby, cast)

          c:move_to(wax, way)
          c:line_to(wpax, wpay)
          c:line_to(wpbx, wpby)
          c:line_to(wbx, wby)
          c:fill()
        end
      end
    end
  end
end

math.project = function(x, y, x2, y2, dist)
  local dx, dy = x2 - x, y2 - y
  local d = math.sqrt(dx * dx + dy * dy)
  d = 1/d*dist
  return dx*d + x, dy*d + y
end

math.dot = function(ax, ay, bx, by)
  return ax*bx + ay*by
end