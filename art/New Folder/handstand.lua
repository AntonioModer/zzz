local Handstand = {}
local HandstandMT = { __index = Handstand }

Handstand.create = function(self, mt)
	local self = State:create(HandstandMT)

  return self
end
Handstand.destroy = function(self)
end

Handstand.enter = function(self, agent)
  agent:setStance("standing")
end
Handstand.update = function(self, agent, dt)
end
Handstand.exit = function(self, agent)
end

Handstand.message = function(self, agent, msg)
  if msg == "fall" then
    agent:setState("diving")
  elseif msg == "move" then
    agent:setState("handswalk")
  end
end

return Handstand