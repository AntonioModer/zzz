local animations =
{
  waiting = { offset = 6, frames = 2, fps = 0.4, loop = true },
  scanning = { offset = 6, frames = 2, fps = 0.4, loop = true }
}

Guard = {}
GuardMT = { __index = Guard }

setmetatable(Guard, { __index = Agent })

local psq = 16 -- hitbox size
local hs = 2100 -- running acceleration
local as = hs/5.5 -- horizontal jump/fall acceleration
local cs = hs/4 -- crawling acceleration
local vs = 4800 -- vertical jump acceleration
local dm = 2 -- damping
local fr = 0.3 -- friction

Guard.create = function(self, wx, wy)
	local self = Agent:create(wx, wy, GuardMT)
  
  self.xmove = 0
  self.ymove = 0
  self.xdisplace = 0
  self.ydisplace = 0

  local s = fizz.addDynamic("rect", wx, wy + sq/3, psq/2, psq-4)
  s.damping = dm
  s.friction = fr
  s.onCollide = function(s, b, nx, ny, pen)
    -- add up displacement vectors
    self.xdisplace = self.xdisplace + nx*pen
    if ny > 0 then
      self.ydisplace = self.ydisplace + ny*pen
    end
    return true
  end

  self.sprite.color = RED
  self.sprite.depth = 0.6
  self.raycast = { shape = "line", x = wx, y = wx }

  self:setState("waiting")
  self:setShape(s)
  self:setAnimations(runimg, animations)
  
  return self
end
Guard.destroy = function(self)
  self:setShape(nil)
  self:setAnimations(nil, nil)
  Agent.destroy(self)
end


Guard.message = function(self, msg)
  local state = self.state
  if state == "waiting" then
    if msg == "scan" then
      self:setState("scanning")
    end
  elseif state == "scanning" then
    if msg == "wait" then
      self:setState("waiting")
    end
  end
end
Guard.updateState = function(self, dt)
  local state = self.state
  if state == "waiting" then
    if self.elapsed > 0.5 then
      self.clearshot = false
      self:setState("scanning")
    end
  elseif state == "scanning" then
    local s = self.shape
    local ps = p.shape
    local rc = self.raycast
    rc.x = ps.x
    rc.y = ps.y
    rc.x2 = s.x
    rc.y2 = s.y + sq
    local t1 = fizz.testShapes(rc, ps)
    
    for i, v in ipairs(fizz.statics) do
      if v ~= s and fizz.testShapes(rc, v) then
        t1 = false
        break
      end
    end
    rc.x = s.x
    rc.y = s.y + sq
    rc.x2 = ps.x
    rc.y2 = ps.y
    local t2 = fizz.testShapes(rc, ps)
    for i, v in ipairs(fizz.statics) do
      if v ~= s and fizz.testShapes(rc, v) then
        t2 = false
        break
      end
    end
    if t1 or t2 then
      self.clearshot = true
      self.sightedx = ps.x
      self.sightedy = ps.y
    end
    self:message("wait")
  end
  
  local s = self.shape
  local cx, cy = map:posToCell(s.x, s.y)
  if self.facing == 1 then
    s.xv = 30
    if map.map[cx + 1][cy] == 1 or map.map[cx + 1][cy + 1] == 1 or
      map.map[cx + 1][cy - 2] == 0 then
      self.facing = -1
    end
  else
    s.xv = -30
    if map.map[cx - 1][cy] == 1 or map.map[cx - 1][cy + 1] == 1 or
      map.map[cx - 1][cy - 2] == 0 then
      self.facing = 1
    end
  end
end



Guard.sync = function(self, dt)
  Agent.sync(self, dt)
  local s = self.shape
  local c = self.sprite.canvas
  if self.clearshot then
    c:move_to(0, sq)
    c:line_to(self.facing*(self.sightedx - s.x), self.sightedy - s.y)
    c:set_line_style(1, RED, 1)
    c:stroke()
  end
end

return Guard