local LadderClimbing = {}
local LadderClimbingMT = { __index = LadderClimbing }

LadderClimbing.create = function(self, mt)
	local self = State:create(LadderClimbingMT)

  return self
end
LadderClimbing.destroy = function(self)
end

LadderClimbing.enter = function(self, agent)
  self.dir = agent:getDirection()
  local shape = agent:getShape()
  self.x, self.y = shape.x, shape.y
end
LadderClimbing.update = function(self, agent, dt)
  -- freeze
  local dir = agent:getDirection()
  if dir ~= self.dir then
    agent:setDirection(self.dir)
  end
  local shape = agent:getShape()
  shape.x = self.x
  shape.y = self.y

  -- climb
  local mx, my = agent:getMovement()
  if my == 0 then
    self:message("stop")
    return
  end
  local vel = agent:getClimbUpVelocity()
  shape.y = shape.y + my*vel*dt
  self.y = shape.y
  
  fizz.repartition(shape)
  
  if not agent:canClimbLadder() then
    self:message("fall")
    return
  end
end
LadderClimbing.exit = function(self, agent)
  self.dir = nil
  self.x, self.y = nil, nil
end

LadderClimbing.message = function(self, agent, msg)
  if msg == "fall" then
    agent:setState("falling")
  elseif msg == "jump" then
    agent:setState("falling")
  elseif msg == "stop" then
    agent:setState("laddergrab")
  end
end

return LadderClimbing