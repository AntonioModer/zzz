local Handswalk = {}
local HandswalkMT = { __index = Handswalk }

Handswalk.create = function(self, mt)
	local self = State:create(HandswalkMT)

  return self
end
Handswalk.destroy = function(self)
end

Handswalk.enter = function(self, agent)
  self.dir = agent:getDirection()
end
Handswalk.update = function(self, agent, dt)
  local dir = agent:getDirection()
  local accel = agent:getCrawlAcceleration()
  local shape = agent:getShape()
  shape.xv = shape.xv + accel*dir
end
Handswalk.exit = function(self, agent)
  self.dir = nil
end

Handswalk.message = function(self, agent, msg)
  if msg == "fall" then
    agent:setState("diving")
  elseif msg == "stop" then
    agent:setState("handstand")
  end
end

return Handswalk