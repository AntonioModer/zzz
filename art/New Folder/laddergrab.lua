local LadderGrab = {}
local LadderGrabMT = { __index = LadderGrab }

LadderGrab.create = function(self, mt)
	local self = State:create(LadderGrabMT)

  return self
end
LadderGrab.destroy = function(self)
end

LadderGrab.enter = function(self, agent)
  self.dir = agent:getDirection()
  local shape = agent:getShape()
  local cx, cy = map:posToCell((shape.x + 0.5)/sq*sq, shape.y, sq)
  if map:getBit(cx, cy, bits.ladder) then
  elseif map:getBit(cx - 1, cy, bits.ladder) then
    cx = cx - 1
  elseif map:getBit(cx + 1, cy, bits.ladder) then
    cx = cx + 1
  end
  self.x, self.y = map:cellPosC(cx, cy)
end
LadderGrab.update = function(self, agent, dt)
  -- freeze
  local dir = agent:getDirection()
  if dir ~= self.dir then
    agent:setDirection(self.dir)
  end
  local shape = agent:getShape()
  shape.x = self.x
  shape.y = self.y
  
  fizz.repartition(shape)
end
LadderGrab.exit = function(self, agent)
  self.dir = nil
  self.x, self.y = nil, nil
end

LadderGrab.message = function(self, agent, msg)
  if msg == "fall" then
    agent:setState("falling")
  elseif msg == "jump" then
    agent:setState("falling")
  elseif msg == "up" then
    --agent:setState("ladderclimb")
  elseif msg == "down" then
    --agent:setState("ladderclimb")
  end
end

return LadderGrab