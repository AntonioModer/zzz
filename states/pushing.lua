local Pushing = {}
local PushingMT = { __index = Pushing }

Pushing.create = function(self, mt)
	local self = State:create(PushingMT)

  return self
end
Pushing.destroy = function(self)
  State.destroy(self)
end

Pushing.enter = function(self, agent)
  self.dir = agent:getDirection()
end
Pushing.update = function(self, agent, dt)
  local dir = agent:getDirection()
  if dir ~= self.dir then
    agent:message("stop")
    return
  end
  local accel = agent:getDefinition("move")
  local s = agent:getShape()
  s.xv = s.xv + accel*dir
end
Pushing.exit = function(self, agent)
  self.dir = nil
end

Pushing.message = function(self, agent, msg)
  if msg == "fall" then
    agent:setState("falling")
  elseif msg == "jump" then
    agent:setState("jumping")
  elseif msg == "stop" then
    agent:setState("stopping")
  elseif msg == "punch" then
    agent:setState("punching")
  elseif msg == "climb" then
    agent:setState("climbing")
  elseif msg == "down" then
    agent:setState("ducking")
  end
end

return Pushing