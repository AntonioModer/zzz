local Stopping = {}
local StoppingMT = { __index = Stopping }

Stopping.create = function(self, mt)
	local self = State:create(StoppingMT)

  return self
end
Stopping.destroy = function(self)
  State.destroy(self)
end

Stopping.enter = function(self, agent)
end
Stopping.update = function(self, agent, dt)
  local elapsed = agent:getElapsed()
  local xmove, ymove = agent:getMovement()
  local dir = agent:getDirection()
  if xmove ~= 0 then
    if (xmove < 0 and dir < 0) or (xmove > 0 and dir > 0) then
      agent:message("move")
    end
  else
    if elapsed > 0.1 then
      agent:message("up")
    end
  end
end
Stopping.exit = function(self, agent)
end

Stopping.message = function(self, agent, msg)
  if msg == "fall" then
    agent:setState("falling")
  elseif msg == "jump" then
    local xmove, ymove = agent:getMovement()
    local dir = agent:getDirection()
    if (xmove < 0 and dir > 0) or (xmove > 0 and dir < 0) then
      agent:setState("backflip")
    end
  elseif msg == "up" then
    agent:setState("standing")
  elseif msg == "move" then
    agent:setState("running")
  end
end

return Stopping