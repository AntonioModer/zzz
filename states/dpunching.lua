local DPunching = {}
local DPunchingMT = { __index = DPunching }

DPunching.create = function(self, mt)
	local self = State:create(DPunchingMT)

  return self
end
DPunching.destroy = function(self)
  State.destroy(self)
end

DPunching.enter = function(self, agent)
  self.dir = agent:getDirection()
end
DPunching.update = function(self, agent, dt)
  local dir = agent:getDirection()
  if dir ~= self.dir then
    agent:setState("ducking")
    return
  end
  --shape.xv = shape.xv + accel*dir--*dt
  local elapsed = agent:getElapsed()
  if elapsed - dt >= 0.06*3 then
    local s = agent:getShape()
    local cx, cy = map:posToCell(s.x + (sq/2 + 1)*dir, s.y + (sq/4 + 1), sq)
    map:breakCell(cx, cy)
  end
  if elapsed >= 0.06*6 then
    agent:setState("ducking")
  end
end
DPunching.exit = function(self, agent)
  self.dir = nil
end

DPunching.message = function(self, agent, msg)
  if msg == "fall" then
    agent:setState("falling")
  end
end

return DPunching