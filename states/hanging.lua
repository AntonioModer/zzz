local Hanging = {}
local HangingMT = { __index = Hanging }

Hanging.create = function(self, mt)
	local self = State:create(HangingMT)

  return self
end
Hanging.destroy = function(self)
end

Hanging.enter = function(self, agent)
  self.time = 0
end
Hanging.update = function(self, agent, dt)
  
end
Hanging.exit = function(self, agent)
  self.time = nil
end

Hanging.message = function(self, agent, msg)
  if msg == "stop" then
    agent:setState("stopping")
  elseif msg == "fall" then
    agent:setState("falling")
  elseif msg == "jump" then
    agent:setState("backflip")
  end
end

return Hanging