local Falling = {}
local FallingMT = { __index = Falling }

Falling.create = function(self, mt)
	local self = State:create(FallingMT)

  return self
end
Falling.destroy = function(self)
  State.destroy(self)
end

Falling.enter = function(self, agent)
  agent:setStance("flying")
end
Falling.update = function(self, agent, dt)
  local elapsed = agent:getElapsed()
  if elapsed > 1 then
    agent:setState("diving")
  end
  local s = agent:getShape()
  -- horizontal movement
  local xmove, ymove = agent:getMovement()
  local accel = agent:getDefinition("airmove")
  s.xv = s.xv + accel*xmove--*dt
end
Falling.exit = function(self, agent)
end

Falling.message = function(self, agent, msg)
  if msg == "hitground" then
    agent:setState("landing")
  elseif msg == "down" then
    agent:setState("diving")
  elseif msg == "punch" then
    agent:setState("kicking")
  end
end

return Falling