local Barrelroll = {}
local BarrelrollMT = { __index = Barrelroll }

Barrelroll.create = function(self, mt)
	local self = State:create(BarrelrollMT)

  return self
end
Barrelroll.destroy = function(self)
end

Barrelroll.enter = function(self, agent)
  self.elapsed = 0
  self.dir = agent:getDirection()
end
Barrelroll.update = function(self, agent, dt)
  self.elapsed = self.elapsed + dt
  if self.elapsed > 0.06*9 then
    agent:setState("ducking")
    return
  end

  local dir = self.dir
  agent:setDirection(dir)
  local accel = agent:getAcceleration()
  local shape = agent:getShape()
  shape.xv = shape.xv + accel*dir
end
Barrelroll.exit = function(self, agent)
  self.elapsed = nil
  self.dir = nil
end

Barrelroll.message = function(self, agent, msg)
  if msg == "fall" then
    agent:setState("falling")
  end
end

return Barrelroll