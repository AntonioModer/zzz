local Ducking = {}
local DuckingMT = { __index = Ducking }

Ducking.create = function(self, mt)
	local self = State:create(DuckingMT)

  return self
end
Ducking.destroy = function(self)
  State.destroy(self)
end

Ducking.enter = function(self, agent)
  agent:setStance("ducking")
end
Ducking.update = function(self, agent, dt)
  
end
Ducking.exit = function(self, agent)
end

Ducking.message = function(self, agent, msg)
  if msg == "fall" then
    agent:setState("falling")
  elseif msg == "move" then
    agent:setState("crawling")
  elseif msg == "punch" then
    agent:setState("dpunching")
  elseif msg == "jump" then
    agent:setState("rolling")
  elseif msg == "up" then
    if not agent:canStand() then
      return
    end
    agent:setState("standing")
  end
end

return Ducking