local Getup = {}
local GetupMT = { __index = Getup }

Getup.create = function(self, mt)
	local self = State:create(GetupMT)

  return self
end
Getup.destroy = function(self)
end

Getup.enter = function(self, agent)
  self.time = 0
end
Getup.update = function(self, agent, dt)
  self.time = self.time + dt
  if self.time > 0.5 then
    agent:setState("ducking")
  end
end
Getup.exit = function(self, agent)
  self.time = nil
end

Getup.message = function(self, agent, msg)
end

return Getup