local Backflip = {}
local BackflipMT = { __index = Backflip }

Backflip.create = function(self, mt)
	local self = State:create(BackflipMT)

  return self
end
Backflip.destroy = function(self)
  State.destroy(self)
end

Backflip.enter = function(self, agent)
  self.dir = agent:getDirection()
  agent:setStance("flying")
end
Backflip.update = function(self, agent, dt)
  local elapsed = agent:getElapsed()
  local t = agent:getDefinition("backflip")
  if elapsed > t then
    agent:message("fall")
    return
  end
  -- vertical movement
  local r = elapsed/t
  r = 1 - math.sin(math.pi/2*r)
  local vaccel = agent:getDefinition("jump")
  local s = agent:getShape()
  s.yv = s.yv + vaccel*r

  -- horizontal movement
  local xmove, ymove = agent:getMovement()
  local haccel = agent:getDefinition("airmove")
  s.xv = s.xv + haccel*xmove
end
Backflip.exit = function(self, agent)
  self.dir = nil
end

Backflip.message = function(self, agent, msg)
  if msg == "fall" then
    agent:setState("falling")
  elseif msg == "hitroof" then
    agent:setState("diving")
  end
end

return Backflip