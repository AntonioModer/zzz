local Diving = {}
local DivingMT = { __index = Diving }

Diving.create = function(self, mt)
	local self = State:create(DivingMT)

  return self
end
Diving.destroy = function(self)
  State.destroy(self)
end

Diving.enter = function(self, agent)
end
Diving.update = function(self, agent, dt)
  
end
Diving.exit = function(self, agent)
end

Diving.message = function(self, agent, msg)
  if msg == "hitground" then
    agent:setState("crashing")
  end
end

return Diving