local Rising = {}
local RisingMT = { __index = Rising }

Rising.create = function(self, mt)
	local self = State:create(RisingMT)

  return self
end
Rising.destroy = function(self)
  State.destroy(self)
end

Rising.enter = function(self, agent)
end
Rising.update = function(self, agent, dt)
  local elapsed = agent:getElapsed()
  if elapsed > 5*0.08 then
    agent:setState("ducking")
  end
end
Rising.exit = function(self, agent)
end

Rising.message = function(self, agent, msg)
  if msg == "fall" then
    agent:setState("falling")
  end
end

return Rising