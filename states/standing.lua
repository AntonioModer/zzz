local Standing = {}
local StandingMT = { __index = Standing }

Standing.create = function(self, mt)
	local self = State:create(StandingMT)

  return self
end
Standing.destroy = function(self)
  State.destroy(self)
end

Standing.enter = function(self, agent)
  agent:setStance("standing")
end
Standing.update = function(self, agent, dt)
  local jump = agent:getJump()
  if jump > 0 then
    agent:message("jump")
    return
  end
  local xmove, ymove = agent:getMovement()
  if xmove ~= 0 then
    local dir = agent:getDirection()
    if (xmove < 0 and dir < 0) or (xmove > 0 and dir > 0) then
      agent:message("stop")
    else
      agent:message("move")
    end
  end
end
Standing.exit = function(self, agent)
end

Standing.message = function(self, agent, msg)
  if msg == "fall" then
    agent:setState("falling")
  elseif msg == "move" then
    agent:setState("running")
  elseif msg == "jump" then
    agent:setState("jumping")
  elseif msg == "down" then
    agent:setState("ducking")
  elseif msg == "punch" then
    agent:setState("punching")
  end
end

return Standing