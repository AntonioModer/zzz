local Rolling = {}
local RollingMT = { __index = Rolling }

Rolling.create = function(self, mt)
	local self = State:create(RollingMT)

  return self
end
Rolling.destroy = function(self)
  State.destroy(self)
end

Rolling.enter = function(self, agent)
  self.dir = agent:getDirection()
end
Rolling.update = function(self, agent, dt)
  local elapsed = agent:getElapsed()
  if elapsed > 0.06*9 then
    agent:setState("ducking")
    return
  end

  local dir = self.dir
  agent:setDirection(dir)
  local accel = agent:getDefinition("move")
  local s = agent:getShape()
  s.xv = s.xv + accel*dir
end
Rolling.exit = function(self, agent)
  self.dir = nil
end

Rolling.message = function(self, agent, msg)
  if msg == "fall" then
    agent:setState("falling")
  end
end

return Rolling