local Jumping = {}
local JumpingMT = { __index = Jumping }

Jumping.create = function(self, mt)
	local self = State:create(JumpingMT)

  return self
end
Jumping.destroy = function(self)
  State.destroy(self)
end

Jumping.enter = function(self, agent)
  agent:setStance("flying")
end
Jumping.update = function(self, agent, dt)
  local elapsed = agent:getElapsed()
  local jtime = agent:getDefinition("jumptime")
  if elapsed > jtime then
    agent:setState("falling")
    return
  end
  -- vertical movement
  local r = elapsed/jtime
  r = 1 - math.sin(math.pi/2*r)
  local vaccel = agent:getDefinition("jump")
  local s = agent:getShape()
  s.yv = s.yv + vaccel*r

  -- horizontal movement
  local jump = agent:getJump()
  if jump <= 0 then
    agent:setState("falling")
    return
  end
  local xmove, ymove = agent:getMovement()
  local dir = agent:getDirection()
  local haccel = agent:getDefinition("airmove")
  s.xv = s.xv + haccel*xmove
end
Jumping.exit = function(self, agent)
end

Jumping.message = function(self, agent, msg)
  if msg == "punch" then
    agent:setState("kicking")
  elseif msg == "hitroof" then
    agent:setState("diving")
  end
end

return Jumping