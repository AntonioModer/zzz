local Landing = {}
local LandingMT = { __index = Landing }

Landing.create = function(self, mt)
	local self = State:create(LandingMT)

  return self
end
Landing.destroy = function(self)
  State.destroy(self)
end

Landing.enter = function(self, agent)
  self.yv = agent.yvold or 0
  agent:setStance("standing")
end
Landing.update = function(self, agent, dt)
  local elapsed = agent:getElapsed()
  local dy = math.abs(self.yv)
  dy = math.min(dy/400, 1)
  dy = dy^3
  local ltime = dy*0.7
  if elapsed > ltime then
    agent:setState("standing")
    return
  end
end
Landing.exit = function(self, agent)
  self.yv = nil
end

Landing.message = function(self, agent, msg)
  if msg == "fall" then
    agent:setState("falling")
  end
end

return Landing