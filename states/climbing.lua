local Climbing = {}
local ClimbingMT = { __index = Climbing }

Climbing.create = function(self, mt)
	local self = State:create(ClimbingMT)

  return self
end
Climbing.destroy = function(self)
  State.destroy(self)
end

Climbing.enter = function(self, agent)
  self.dir = agent:getDirection()
end
Climbing.update = function(self, agent, dt)
  local dir = agent:getDirection()
  if dir ~= self.dir then
    agent:message("stop")
    return
  end
  --shape.xv = shape.xv + accel*dir--*dt
  local elapsed = agent:getElapsed()
  if elapsed >= 0.06*6 then
    local shape = agent:getShape()
    fizz.moveShape(shape, sq*dir, sq)
    agent:setState("standing")
    agent:message("down")
  end
end
Climbing.exit = function(self, agent)
  self.dir = nil
end

Climbing.message = function(self, agent, msg)
  if msg == "fall" then
    agent:setState("falling")
  elseif msg == "stop" then
    if not agent:canStand() then
      agent:setState("ducking")
      return
    end
    agent:setState("standing")
  end
end

return Climbing