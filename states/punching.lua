local Punching = {}
local PunchingMT = { __index = Punching }

Punching.create = function(self, mt)
	local self = State:create(PunchingMT)

  return self
end
Punching.destroy = function(self)
  State.destroy(self)
end

Punching.enter = function(self, agent)
  self.dir = agent:getDirection()
end
Punching.update = function(self, agent, dt)
  local dir = agent:getDirection()
  if dir ~= self.dir then
    agent:setState("standing")
    return
  end
  local elapsed = agent:getElapsed()
  if elapsed - dt >= 0.06*3 then
    local s = agent:getShape()
    local lx, ly = (sq/2 + 1)*dir, sq/2 + 1
    local cx, cy = map:posToCell(s.x + lx, s.y + ly, sq)
    map:breakCell(cx, cy)
  end
  if elapsed >= 0.06*6 then
    agent:setState("standing")
  end
end
Punching.exit = function(self, agent)
  self.dir = nil
end

Punching.message = function(self, agent, msg)
  if msg == "fall" then
    agent:setState("falling")
  end
end

return Punching