local Running = {}
local RunningMT = { __index = Running }

Running.create = function(self, mt)
	local self = State:create(RunningMT)

  return self
end
Running.destroy = function(self)
  State.destroy(self)
end

Running.enter = function(self, agent)
  self.dir = agent:getDirection()
end
Running.update = function(self, agent, dt)
  local dir = agent:getDirection()
  local accel = agent:getDefinition("move")
  local s = agent:getShape()
  s.xv = s.xv + accel*dir
  assert(s.friction > 0)
  if self.dir ~= dir then
    agent:message("turn")
  end
end
Running.exit = function(self, agent)
  self.dir = nil
end

Running.message = function(self, agent, msg)
  if msg == "fall" then
    agent:setState("falling")
  elseif msg == "jump" then
    agent:setState("jumping")
  elseif msg == "push" then
    agent:setState("pushing")
  elseif msg == "punch" then
    agent:setState("punching")
  elseif msg == "stop" then
    agent:setState("stopping")
  elseif msg == "climb" then
    agent:setState("climbing")
  elseif msg == "down" then
    agent:setState("ducking")
  elseif msg == "turn" then
    agent:setState("turning")
  end
end

return Running