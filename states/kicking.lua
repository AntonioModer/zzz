local Kicking = {}
local KickingMT = { __index = Kicking }

Kicking.create = function(self, mt)
	local self = State:create(KickingMT)

  return self
end
Kicking.destroy = function(self)
  State.destroy(self)
end

Kicking.enter = function(self, agent)
  agent:setStance("flying")
  self.dir = agent:getDirection()
end
Kicking.update = function(self, agent, dt)
  local elapsed = agent:getElapsed()
  if elapsed > 1 then
    agent:setState("falling")
    return
  end
  -- horizontal movement
  local s = agent:getShape()
  local dir = self.dir
  agent:setDirection(dir)
  if elapsed < 0.2 then
    local accel = agent:getDefinition("airmove")*2
    s.xv = s.xv + accel*dir
  end
  if elapsed - dt >= 0.05*3 then
    local dir = agent:getDirection()
    local lx, ly = (sq/2 + 1)*dir, sq/2 - 2
    local cx, cy = map:posToCell(s.x + lx, s.y + ly, sq)
    if map:breakCell(cx, cy) == true then
      -- bounce off
      agent:setState("stumbling")
      return
    end
  end
end
Kicking.exit = function(self, agent)
  self.dir = nil
end

Kicking.message = function(self, agent, msg)
  if msg == "hitground" then
    agent:setState("landing")
  elseif msg == "down" then
    agent:setState("diving")
  end
end

return Kicking