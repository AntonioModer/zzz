local Crawling = {}
local CrawlingMT = { __index = Crawling }

Crawling.create = function(self, mt)
	local self = State:create(CrawlingMT)

  return self
end
Crawling.destroy = function(self)
  State.destroy(self)
end

Crawling.enter = function(self, agent)
end
Crawling.update = function(self, agent, dt)
  local dir = agent:getDirection()
  local accel = agent:getDefinition("crawl")
  local s = agent:getShape()
  s.xv = s.xv + accel*dir--*dt
end
Crawling.exit = function(self, agent)
end

Crawling.message = function(self, agent, msg)
  if msg == "fall" then
    agent:setState("falling")
  elseif msg == "punch" then
    agent:setState("dpunching")
  elseif msg == "stop" then
    agent:setState("ducking")
  elseif msg == "climb" then
    agent:setState("climbing")
  elseif msg == "jump" then
    agent:setState("rolling")
  elseif msg == "up" then
    if not agent:canStand() then
      return
    end
    agent:setState("standing")
  end
end

return Crawling