local Stumbling = {}
local StumblingMT = { __index = Stumbling }

Stumbling.create = function(self, mt)
	local self = State:create(StumblingMT)

  return self
end
Stumbling.destroy = function(self)
  State.destroy(self)
end

Stumbling.enter = function(self, agent)
  agent:setStance("flying")
  self.dir = agent:getDirection()
end
Stumbling.update = function(self, agent, dt)
  local elapsed = agent:getElapsed()
  if elapsed > 1 then
    agent:setState("diving")
    return
  end
  local s = agent:getShape()
  -- horizontal movement
  local dir = self.dir
  agent:setDirection(dir)
  local accel = agent:getDefinition("airmove")/2
  s.xv = s.xv - accel*dir
end
Stumbling.exit = function(self, agent)
  self.dir = nil
end

Stumbling.message = function(self, agent, msg)
  if msg == "hitground" then
    agent:setState("landing")
  elseif msg == "down" then
    agent:setState("diving")
  end
end

return Stumbling