local Turning = {}
local TurningMT = { __index = Turning }

Turning.create = function(self, mt)
	local self = State:create(TurningMT)

  return self
end
Turning.destroy = function(self)
  State.destroy(self)
end

Turning.enter = function(self, agent)
end
Turning.update = function(self, agent, dt)
  local elapsed = agent:getElapsed()
  local t = agent:getDefinition("turn")
  if elapsed > t then
    local xmove, ymove = agent:getMovement()
    local dir = agent:getDirection()
    if xmove == 0 then
      agent:message("stop")
    else
      if (xmove < 0 and dir < 0) or (xmove > 0 and dir > 0) then
        agent:setState("running")
      end
    end
  end
end
Turning.exit = function(self, agent)
end

Turning.message = function(self, agent, msg)
  if msg == "stop" then
    agent:setState("stopping")
  elseif msg == "fall" then
    agent:setState("falling")
  elseif msg == "jump" then
    agent:setState("backflip")
  end
end

return Turning