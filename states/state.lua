State = {}
StateMT = { __index = State }

State.create = function(self, mt)
	local self = {}
	setmetatable(self, mt or StateMT)

  return self
end
State.destroy = function(self)
end

State.enter = function(self, agent)
end
State.update = function(self, agent, dt)
end
State.exit = function(self, agent)
end

State.message = function(self, agent, msg)
end

return State