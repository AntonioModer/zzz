local Crashing = {}
local CrashingMT = { __index = Crashing }

Crashing.create = function(self, mt)
	local self = State:create(CrashingMT)

  return self
end
Crashing.destroy = function(self)
  State.destroy(self)
end

Crashing.enter = function(self, agent)
  agent:setStance("standing")
  self.yv = agent.yvold or 0
end
Crashing.update = function(self, agent, dt)
  --agent:setStance("ducking")
  local elapsed = agent:getElapsed()
  local dy = math.abs(self.yv)
  dy = math.min(dy/200, 1)
  dy = dy^3
  local ltime = dy*0.7
  if elapsed > ltime then
    agent:setState("rising")
    return
  end
end
Crashing.exit = function(self, agent)
  self.yv = nil
end

Crashing.message = function(self, agent, msg)
  if msg == "fall" then
    agent:setState("falling")
  end
end

return Crashing